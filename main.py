# -*- coding: utf-8 -*-
"""
Created on Sat Jun  4 15:20:27 2016

This script can be used to generate the latexdiff of two versions of a arxiv
preprint manuscript. It will fetch the appropriate versions, run latexdiff,
compile the document and display it in the default viewer of the system.
Tested on linux, might or might not work on windows.

Depends on latexdiff and pdflatex.

Mind that by running this script you basically fetch and execute untrusted code
from others that might do serious damage to your system. To be absolute clear:

###############################################################################
USE AT YOUR OWN RISK! YOU ARE EXECUTING POTENTIALLY HARMFUL CODE ON YOUR SYSTEM
###############################################################################

@author: Hannes Maier-Flaig
"""
from lxml import html
import requests
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
import tarfile
import urllib.request
import tempfile
from glob import glob
import os
import sys
import subprocess


class Preprint(object):
    def __init__(self, arxiv_id=None):
        # FIXME: parse and check if arxiv_id is valid
        self.arxiv_id = str(arxiv_id)
        self._versions = []
        self._version_folders = []

    @property
    def versions(self):
        if len(self._versions) == 0:
            page = requests.get('http://arxiv.org/abs/' + self.arxiv_id)
            tree = html.fromstring(page.content)
            class_name = "submission-history"
            version_objects = tree.xpath('//div[@class="%s"]/b' % class_name)
            versions = []
            for vo in version_objects:
                versions.append(vo.text_content()[1:-1])
            self._versions = versions
            self._version_folders = [None for v in versions]

        return self._versions

    def _reset_state(self):
        """ Reset all fetched values, fetching them again on the next call """
        self._versions = []
        self._version_folders = []

    def get_version_source_urls(self):
        base_url = "http://arxiv.org/e-print/%s" % self.arxiv_id
        urls = [base_url + version_id for version_id in self.versions]
        return urls

    def fetch_version(self, id):
        """
        Downloads and extracts a version identified by the numeric "id"
        and returns the extracted path.
        """
        # TODO: comprehensive error handling
        if len(self._version_folders) <= id or self._version_folders[id] is None:
            tempfolder = tempfile.mkdtemp("_" + self.arxiv_id + self.versions[id])
            with urllib.request.urlopen(self.get_version_source_urls()[id]) as r:
                t = tarfile.open(fileobj=r, mode="r|gz")
                t.extractall(path=tempfolder)
            self._version_folders[id] = tempfolder
            return tempfolder
        else:
            return self._version_folders[id]

    def fetch_versions(self):
        """ Download and extract all versions."""
        for id, version in enumerate(self.versions):
            self.fetch_version(id)
        return self._version_folders

    def diff_texname(self, id1, id2):
        """ Compose name of the generated diff tex file """
        diff_texname = "{aid}_{v1}-{v2}_diff.tex"
        diff_texname = diff_texname.format(aid=self.arxiv_id,
                                           v1=self.versions[id1],
                                           v2=self.versions[id2])
        return diff_texname

    def view_command(self, id1, id2):
        return "xdg-open %s.pdf" % self.diff_texname(id1, id2)[:-4]

    def diff_command(self, id1, id2):
        v2 = self.fetch_version(id2)
        v1_tex = self._get_main_texfile(id1)
        v2_tex = self._get_main_texfile(id2)

        command = "latexdiff -b {v1_tex} {v2_tex} > {v2_folder}/{diff_texname}"
        return command.format(v1_tex=v1_tex, v2_tex=v2_tex, v2_folder=v2,
                              diff_texname=self.diff_texname(id1, id2))

    def compile_command(self, id1, id2):
        compile_command = "cd {folder}; pdflatex -interaction=nonstopmode {folder}/{tex}"
        return compile_command.format(folder=self.fetch_version(id2),
                                      tex=self.diff_texname(id1, id2))

    def _get_main_texfile(self, id):
        """ Guess the main tex-file-name for a given numeric id """
        folder = self.fetch_version(id)
        fnames = glob(folder + "/*.tex")
        return fnames[0]

    def diff_versions(self, id1, id2=-1):
        """
        Display the latexdiff of two versions identified by the numeric ids
        id1 and id2.
        If id2 is -1, the most current version is taken as id2.
        """

        if id2 == -1:
            id2 = len(self.versions) - 1

        print("Fetching versions %s (old) and %s (new)" % (self.versions[id1],
                                                           self.versions[id2]))
        self.fetch_version(id1)
        print("Download complete for %s" % self.versions[id1])
        self.fetch_version(id2)
        print("Download complete for %s" % self.versions[id2])

        print("excecuting diff command: " + self.diff_command(id1, id2))
        os.system(self.diff_command(id1, id2))
        print("excecuting compile command: " + self.compile_command(id1, id2))
        os.system(self.compile_command(id1, id2))

        fname = self.diff_texname(id1, id2)[:-4] + ".pdf"
        print("view: " + self.diff_texname(id1, id2)[:-4] + ".pdf")
        if sys.platform == 'linux':
            subprocess.call(["xdg-open", self.fetch_version(id2) + "/" + fname])
        else:
            os.startfile(fname)


p = Preprint("1601.05681")
p.diff_versions(0, -1)
